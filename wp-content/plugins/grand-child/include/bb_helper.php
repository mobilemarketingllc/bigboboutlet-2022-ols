<?php

add_action( 'init', 'register_bb_cron_delete_event');

// Function which will register the event
function register_bb_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_brand_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_brand_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_hook_activation');

/**  Function for activate cron job  **/
function bb_hook_activation() {
    if (! wp_next_scheduled ( 'bb_brand_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_brand_sync');
    }
}


add_action( 'bb_brand_sync', 'bb_brand_function' );

function bb_brand_function() {
    
    $product_json =  json_decode(get_option('product_json'));

    if(is_array($product_json) && count($product_json) > 0){
       
        $vendor_page = get_page_by_path( '/vendor/', OBJECT, 'page' );
        
        $parent = new WP_Query(array('post_type' =>'page','post_parent' => $vendor_page->ID ,'post_per_page'=>-1));


        if ($parent->have_posts()) : ?>

            <?php while ($parent->have_posts()) : $parent->the_post(); 
                $post = array( 'ID' => get_the_ID(), 'post_status' => "draft" );
                wp_update_post($post);
            endwhile;
            unset($parent); 
        endif; 
        wp_reset_postdata();
        
        foreach($product_json as $product_detail){
            if(isset($product_detail->manufacturer)){
                $check_brand_lp = get_page_by_title( trim($product_detail->manufacturer) );
                if(isset($check_brand_lp) && get_post_status($check_brand_lp->ID) != "publish"){
                    $post = array( 'ID' => $check_brand_lp->ID, 'post_status' => "publish" );
                    wp_update_post($post); 
                }
            }
        }
    } 
}

// Vendor menu list
function bb_brand_vendor_items() {
    ob_start();
    $vendor_page = get_page_by_path( '/vendor/', OBJECT, 'page' );
    ?> 
    <ul class="vendor_brand_menu_list">
    <?php
        wp_list_pages( array(
            'title_li'    => '',
            'child_of'    => $vendor_page->ID,
            'show_date'   => 'modified',
            'date_format' => $date_format
        ) );
    ?>
    </ul>
    <?php
    return ob_get_clean();
}
add_shortcode( 'BRAND_VENDOR_ITEMS', 'bb_brand_vendor_items' );

//CDE location Sync process


add_action( 'init', 'register_bbsync_cron_delete_event');

// Function which will register the event
function register_bbsync_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_location_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_location_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_sync_hook_activation');

/**  Function for activate cron job  **/
function bb_sync_hook_activation() {
    if (! wp_next_scheduled ( 'bb_location_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_location_sync');
    }
}


add_action( 'bb_location_sync', 'bb_location_sync_function' );

function bb_location_sync_function() {

    global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

             // write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                //  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}



// Product landing page product list

function product_category_products_function($atts){

    $args = array(
        "post_type" => $atts[0],
        "post_status" => "publish",
        "orderby" => "title",
        "order" => "rand",
        "posts_per_page" => 4
  
      );

      if($atts[0] == 'tile_catalog'){
        $args['meta_query'] = array(
                array(
                    'key' => 'brand_facet',
                    'value' => 'daltile',
                    'compare' => '='
                )
            );
    }else if($atts[0] == 'carpeting'){
        $args['meta_query'] = array(
            array(
                'key' => 'brand_facet',
                'value' => 'mohawk',
                'compare' => '='
            )
        );
    }
    $the_query = new WP_Query( $args );

    if($atts[0] == 'tile_catalog' || $atts[0] == 'carpeting'){
        if(!$the_query->have_posts()){
        unset($args['meta_query']);
        $the_query = new WP_Query( $args );
        }
    }
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper landing_page_wrapper">				
				<div class="row product-row">';				
								
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'">';
                          
                          if( get_field('quick_ship') == 1){ 

                            $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';

                                } 

                       $content .='</a>'.brandroomvo_script_integration(get_field('manufacturer') ,get_field('sku') ,get_the_ID()).'</div>';

    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"><span class="color_text"> 
                            '.get_field('color').' </span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>            
                </div>
            </div>';
           
    } 
       
 wp_reset_postdata(); 
						
   $content .= '</div>
			</div>';
            
            return $content;

} 

// Diff collection products

add_shortcode( 'product_listing', 'product_category_products_function' );

function search_distinct() {    global $wpdb;

    return $wpdb->postmeta . '.meta_value ';  }


    
// Vendor page product list

function brand_products_function($atts){

    global $wpdb;

    //write_log( $atts);
    $args = array(
        "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
        "post_status" => "publish",       
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value', 
        "posts_per_page" => 4,
      'meta_query' => array(
                array(
                  'key' => 'brand',
                  'value' => $atts[0],
                  'compare' => '='
                  )
         )
      );

    //  write_log( $args);
    add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';				
                                
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'"/>';
                          
                            if( get_field('quick_ship') == 1){ 
  
                              $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';
  
                                  } 
  
                         $content .='</a>'.brandroomvo_script_integration(get_field('manufacturer') ,get_field('sku') ,get_the_ID()).'</div>';

                    $table_posts = $wpdb->prefix.'posts';
                    $table_meta = $wpdb->prefix.'postmeta';
                    $familycolor = get_field('collection');  
                    $key = 'collection';  

                    $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

                           //     write_log($coll_sql);

                    $data_collection = $wpdb->get_results($coll_sql);

                    
    
    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"> <span class="color_text">'.get_field('color').'</span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>'.count($data_collection).' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
           
    } 
       
    wp_reset_postdata(); 
    
    remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';
            
            return $content;
    
    } 
    
    add_shortcode( 'brand_listing', 'brand_products_function' );



    
//Accessibility employee shortcode
function accessibility_shortcodes($arg) {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->contacts as $contact){

        if($contact->accessibility == '1'){ 

            if (in_array("phone", $arg)) {

                $content = '<a href="tel:'.$contact->phone.'">'.$contact->phone.'</a>';                
               
            }
            if (in_array("email", $arg)) {

                $content = '<a href="mailto:'.$contact->email.'">'.$contact->email.'</a>';              
               
            }
        }
    }

    return  $content;
    
}
add_shortcode('accessibility', 'accessibility_shortcodes');

///Make my location functionality of header

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );

function get_storelisting() {
  
    global $wpdb;
    $content="";
  
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );
        
        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];
  
        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
  
       //print_r($rawdata);
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";
  
              // write_log($sql);
  
        $storeposts = $wpdb->get_results($sql);
        $storeposts_array = json_decode(json_encode($storeposts), true);     
        
        
  
        if(isset($_COOKIE['preferred_store'])){
  
            $store_location = $_COOKIE['preferred_store'];
           

        }else{ 
            $store_location = $storeposts_array['0']['ID'];          
            
           
          }
      
          
          $store_title =  get_the_title( $store_location);

        $content = '<div class="bb_location_name">'.$store_title.'</div>
        <div class="bb_loc_address">'.get_post_meta( $store_location, 'address', true ).' '.get_post_meta( $store_location, 'city', true ).', '.get_post_meta( $store_location, 'state', true ).' '.get_post_meta( $store_location, 'postal_code', true ).'</div>';     
        

        $content_list = "";

    foreach ( $storeposts as $post ) { 

        

        if($_COOKIE['preferred_store'] == $post->ID){

            $class = "activebb";

        }else{

            $class = "";
        }
        
                 
                  $content_list .= '<div class="location-section '.$class.'" id ="'.$post->ID.'">
                  <div class="makemystore_header">                  
                  <input type="radio" data-store-id"="'.$post->ID.'" id="'.$post->ID.'_mystore" class="mystore_radio makemystore_radio" name="mystore" value="'.$post->ID.'">
                  <label for="'.$post->ID.'_mystore"></label>
                  </div>
                  <div class="location-info-section">
                      <a href="'.get_post_meta( $post->ID, 'store_link', true ).'" class="location-name">'.get_the_title( $post->ID).' - '.round($post->distance,1).' MI</a>
                      <p class="store-add"> '.get_post_meta( $post->ID, 'address', true ).' '.get_post_meta( $post->ID, 'city', true ).', '.get_post_meta( $post->ID, 'state', true ).' '.get_post_meta( $post->ID, 'postal_code', true ).'</p>';

                      if(get_post_meta( $post->ID, 'phone', true ) != ''){
                      $content_list .= '<p class="store-phone"><a class="phone_bb" href="tel:'.get_post_meta( $post->ID, 'phone', true ).'">'.get_post_meta( $post->ID, 'phone', true ).'</a></p>';
                      }

                      $content_list .=  '</div>
              </div>'; 
    } 
    
  
    $data = array();
  
    $data['header'] = $content;
    $data['list'] = $content_list;
    $data['store_location'] = get_the_title( $post->ID);
    $data['phone'] = formatPhoneNumber(get_post_meta( $post->ID, 'phone', true ));
    $data['distance'] = round($post->distance,1);
    $data['store_name'] = get_the_title( $post->ID);
    $data['address'] =  get_post_meta( $post->ID, 'address', true ).''.get_post_meta( $post->ID, 'city', true ).', '.get_post_meta( $post->ID, 'state', true ).' '.get_post_meta( $post->ID, 'zip', true );
    $data['store_id']=  $store_location;

   // write_log($data['list']);
  
    echo json_encode($data);
        wp_die();
  }


  //Quick ship sync

add_action( 'init', 'register_bb_quickship_cron_delete_event');

// Function which will register the event
function register_bb_quickship_cron_delete_event() {
	if( !wp_next_scheduled( 'bb_quickship_sync' ) ) {
        // Schedule the event
    	wp_schedule_event(time(), 'daily', 'bb_quickship_sync' );
    }
}
   

register_activation_hook(__FILE__, 'bb_quickship_hook_activation');

/**  Function for activate cron job  **/
function bb_quickship_hook_activation() {
    if (! wp_next_scheduled ( 'bb_quickship_sync' )) {
	    wp_schedule_event(time(), 'daily', 'bb_quickship_sync');
    }
}


add_action( 'bb_quickship_sync', 'bb_quickship_sync_function' );

function bb_quickship_sync_function(){

    global $wpdb;
    $upload_dir = wp_upload_dir(); 
    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_group = $wpdb->prefix.'redirection_groups';		
    $table_posts = $wpdb->prefix.'posts';
    $table_meta = $wpdb->prefix.'postmeta';
    
    $permfile = $upload_dir.'/quickship.csv';

    $FileContent = @fopen($upload_dir['basedir'].'/'.'sfn-data/quickship.csv', "r");

   // write_log($FileContent);

    $i=0;

    if ($FileContent) {

            while (($row = fgetcsv($FileContent, 1000)) !== false) {

                if($row[1] !='' && $row[0] == 1){

                    $find_sku = $row[1];

                    $sql_sku = "SELECT $table_meta.post_id 
                    FROM $table_meta
                    WHERE  $table_meta.meta_key = 'sku' 
                    AND $table_meta.meta_value = '$find_sku'" ;	
                    
                    $sku_result = $wpdb->get_results($sql_sku,ARRAY_A);	
                
                        if (count($sku_result)> 0){
                
                            write_log('Product in Catalog-'.$find_sku);

                            $post_id = $sku_result[0]['post_id'];
                            
                            update_post_meta($post_id, 'quick_ship', '1'); 
                
                        }else{

                            write_log('Product not in Catalog-'.$find_sku);

                        }

                }

                    $i++;
            }

        }
}

add_filter( 'gform_pre_render_4', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_4', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_4', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_4', 'populate_product_location_form' );

add_filter( 'gform_pre_render_11', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_11', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_11', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_11', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if( $field['id'] == 11 || $field['id'] == 14 ) {           	

            $args = array(
                'post_type'      => 'store-locations',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

             // write_log($choices);

             // Set placeholder text for dropdown
             $field->placeholder = '-- Choose Location --';

             // Set choices from array of ACF values
             $field->choices = $choices;

        }
   }
return $form;

}


//make_my_store FUnctionality

add_action( 'wp_ajax_nopriv_make_my_store', 'make_my_store' );
add_action( 'wp_ajax_make_my_store', 'make_my_store' );

function make_my_store() {
    
     $data = array(); 
     $store_title = get_the_title( $_POST['store_id']);      
     $store_address = get_post_meta( $_POST['store_id'], 'address',true ).' '.get_post_meta($_POST['store_id'],'city',true).', '.get_post_meta($_POST['store_id'],'state',true).' '.get_post_meta($_POST['store_id'],'postal_code',true);
     $data['store_title']= get_the_title( $_POST['store_id']); 
     $data['header_phone'] = formatPhoneNumber(get_post_meta( $_POST['store_id'], 'phone',true ));
     $data['store_address']= $store_address;       
   
     echo json_encode($data);    	
    
     wp_die();
}





// Area Rug Gallery Images
function bb_financing_page_function(){

    $website_json =  json_decode(get_option('website_json'));
    
    if(isset($website_json->financeWF) && $website_json->financeWF!=''){
      
        echo do_shortcode('[fl_builder_insert_layout slug="wellsfargo_financing"]');

    }elseif(isset($website_json->financeSync) && $website_json->financeSync!=''){
        
        echo do_shortcode('[fl_builder_insert_layout slug="synchrony_financing"]');

    }elseif(isset($website_json->financeGeneric) && $website_json->financeGeneric == 1 ){
       
        echo do_shortcode('[fl_builder_insert_layout slug="generic_financing"]');

    }
    
}
add_shortcode('bb_financing', 'bb_financing_page_function');

//Roomvo script integration
function brandroomvo_script_integration($manufacturer,$sku,$pro_id){

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){

                    if($manufacturer == 'Bruce'){ 
                      $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer == 'AHF'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer  == 'Shaw'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    else{
                        $manufacturer == $manufacturer ;
                    }

                    $manufacturer = str_replace(' ', '', strtolower($manufacturer));  

                    $content='<div id="roomvo">
                            <div class="roomvo-container">
                        <a class="roomvo-stimr button" data-sku="'.$manufacturer.'-'.$sku.'" style="visibility: hidden;"><i class="fa fa-camera" aria-hidden="true"></i><span class="room_span"> &nbsp;VIEW IN ROOM</span></a>
                            </div>
                    </div>';
       
               $content .='<script type="text/javascript">
                               function getProductSKU() {                       
                               return "'.$manufacturer.'-'.$sku.'";
                               }
                           </script>';
        
                 return $content;
    
            }
    
        }
    }
   
}

// Vendor menu list
function bb_brand_image_items() {
    ob_start();
    // $vendor_page = get_page_by_path( '/vendor/', OBJECT, 'page' );
    ?> 
    <ul class="vendor_brand_image_list">
        <li class="brand_section_hide dixie-home"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/dixie_logo.png" alt="dixie_logo" title="dixie_logo"></li>
        <li class="brand_section_hide phenix"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/phenix_logo.png" alt="phenix_logo" title="phenix_logo"></li>
        <li class="brand_section_hide daltile"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/daltile_logo.png" alt="daltile_logo" title="daltile_logo"></li>
        <li class="brand_section_hide engineered-floors"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/engineered_logo.png" alt="engineered_logo" title="engineered_logo"></li>
        <li class="brand_section_hide mannington"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/mannington_logo.png" alt="mannington_logo" title="mannington_logo"></li>
        <li class="brand_section_hide mohawk"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/mohawk_logo.png" alt="mohawk_logo" title="mohawk_logo"></li>
        <li class="brand_section_hide american-olean"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/americanOlean_logo.png" alt="american-olean_logo" title="american-olean_logo"></li>
        <li class="brand_section_hide flooring-on-source"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="flooring-on-source" title="flooring-on-source"></li>
        <li class="brand_section_hide happy-feet"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/happyfeet.png" alt="happy-feet_logo" title="happy-feet_logo"></li>
        <li class="brand_section_hide healthier-choice"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="healthier-choice" title="healthier-choice"></li>
        <li class="brand_section_hide laggett-platt"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="laggett-platt" title="laggett-platt"></li>
        <li class="brand_section_hide marazzi"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/marazzi_logo.jpg" alt="marazzi_logo" title="marazzi_logo"></li>
        <li class="brand_section_hide nance"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="nance_logo" title="nance_logo"></li>
        <li class="brand_section_hide nourison"><img src="https://mmllc-images.s3.amazonaws.com/promos/tstprmtn1/select_3348_nourisonlogo.png" alt="nourison" title="nourison"></li>
        <li class="brand_section_hide peerless-flooring"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/No_image_available.svg" alt="peerless-flooring" title="peerless-flooring"></li>
        <li class="brand_section_hide shaw-flooring"><img src="https://mmllc-images.s3.us-east-2.amazonaws.com/big-bobs-brand-logo/shaw_logo.png" alt="shaw-flooring" title="shaw-flooring"></li>
    </ul>
    <?php
    return ob_get_clean();
}
add_shortcode( 'BRAND_IMAGE_ITEMS', 'bb_brand_image_items' );