<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="fl-content product col-sm-12 <?php //FLTheme::content_class(); ?>">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php 
				if($LAYOUT_COL === 'big_bobs'){
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-big_bobs.php';
				}else if($LAYOUT_COL === "floors_to_ceiling"){
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-floors_to_ceiling.php';
				}else{
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/content-single-product-6.php';
				}
				include( $dir ); 
			?>
				<?php //get_template_part('content', 'single-product'); ?>
			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>
	</div>
</div>

<?php get_footer(); ?>